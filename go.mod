module bitbucket.org/WAN_D_MEAN/harvest-vocab-backend

go 1.16

require (
	cloud.google.com/go/texttospeech v1.0.0
	github.com/99designs/gqlgen v0.14.0
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-chi/chi v1.5.4
	github.com/go-chi/cors v1.2.0
	github.com/joho/godotenv v1.4.0
	github.com/vektah/gqlparser/v2 v2.2.0
	go.mongodb.org/mongo-driver v1.7.4
	golang.org/x/crypto v0.0.0-20211108221036-ceb1ce70b4fa
	google.golang.org/genproto v0.0.0-20211104193956-4c6863e31247
)
