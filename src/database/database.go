package database

import (
	"context"
	"log"
	"time"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/configs"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var (
	Users      *mongo.Collection
	UserVocab  *mongo.Collection
	Vocabulary *mongo.Collection
)

func InitDB() {
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(configs.MONGO_URI))
	if err != nil {
		log.Fatal(err)
	}
	db := client.Database(configs.DATABASE)
	// create collection
	Users = db.Collection("users")
	UserVocab = db.Collection("user_vocabs")
	Vocabulary = db.Collection("vocabularies")
}
