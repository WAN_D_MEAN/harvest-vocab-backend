package auth

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/models/users"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/utils"
)

func Register(w http.ResponseWriter, r *http.Request) {
	// decode request body
	var register users.Register
	json.NewDecoder(r.Body).Decode(&register)

	// check if email is exist
	user := users.FindByEmail(r.Context(), register.Email)
	if user.Email != "" {
		// throw bad request erroor
		utils.ResponseError(w, "email already exist", 400)
		return
	}
	// savev input into database
	user, err := users.Save(r.Context(), register)
	if err != nil {
		// throw internal server error
		utils.ResponseError(w, err.Error(), 500)
		return
	}
	// generate Token
	token, _ := GenerateToken(user.ID.Hex())
	// respnse json
	utils.ResponseJson(w, token, 201)
}

func Login(w http.ResponseWriter, r *http.Request) {
	// decode request body
	var login users.Login
	json.NewDecoder(r.Body).Decode(&login)
	// check if email is exist
	user := users.FindByEmail(r.Context(), login.Email)
	if user.Email == "" {
		// throw bad request error
		utils.ResponseError(w, "user does not exist", 400)
		return
	}
	if users.CheckPasswordHash(login.Password, user.Password) {
		// generate token
		token, _ := GenerateToken(user.ID.Hex())
		// response json
		utils.ResponseJson(w, token, 200)
		return
	}
	// throw unauthorized error
	utils.ResponseError(w, "invalid password", 401)
}

