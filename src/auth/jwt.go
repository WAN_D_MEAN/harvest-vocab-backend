package auth

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/configs"
	"github.com/dgrijalva/jwt-go"
)

var (
	SecretKey = []byte(configs.JWT_SECRET)
)

func GenerateToken(userId string) (string, error) {
	token := jwt.New(jwt.SigningMethodHS256)
	// create a map to store our claims
	claims := token.Claims.(jwt.MapClaims)
	// set token claims
	claims["userId"] = userId
	claims["exp"] = time.Now().Add(time.Hour * 24).Unix()
	tokenString, err := token.SignedString((SecretKey))
	if err != nil {
		log.Fatal("Error in Generating jwt token")
		return "", err
	}
	return tokenString, nil
}

// parses a jwt token and returns the userId in it's claims
func ParseToken(tokenStr string) (string, error) {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return SecretKey, nil
	})
	if err != nil {
		return "", fmt.Errorf(err.Error())
	}
	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		userId := claims["userId"].(string)
		return userId, nil
	} else {
		return "", err
	}
}
