package auth

import (
	"context"
	"html"
	"net/http"
	"strings"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/models/users"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/utils"
)

var UserCtxKey = &contextKey{"user"}

type contextKey struct {
	name string
}

func Middleware(next http.Handler) http.Handler {
	fn := func(w http.ResponseWriter, r *http.Request) {
		// Allow unauthenticated path in
		if UnauthenticatedPath(r.Method + html.EscapeString(r.URL.Path)) {
			next.ServeHTTP(w, r)
			return
		}
		// validate jwt token
		tokenStr := r.Header.Get("Authorization")
		userId, err := ParseToken(tokenStr)
		if err != nil {
			utils.ResponseError(w, "invalid token", 401)
			return
		}

		ctx := r.Context()
		// create user and check if user exists in db
		user, err := users.FindById(ctx, userId)
		if err != nil {
			utils.ResponseError(w, "not found user", 404)
			return
		}
		// put it in context
		ctx = context.WithValue(ctx, UserCtxKey, user)
		// and call the next with our new context
		next.ServeHTTP(w, r.WithContext(ctx))
	}

	return http.HandlerFunc(fn)
}

// ForContext finds the user from the context. REQUIRES Middleware to have run.
func ForContext(ctx context.Context) *users.UserModel {
	raw, _ := ctx.Value(UserCtxKey).(*users.UserModel)
	return raw
}

// Check unauthenticated path
func UnauthenticatedPath(inputPath string) bool {
	unauthPath := []string{
		"post/api/register",
		"post/api/login",
		"get/api/speech",
		"get/",
	}
	for _, path := range unauthPath {
		if path == strings.ToLower((inputPath)) {
			return true
		}
	}
	return false
}
