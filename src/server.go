package main

import (
	"log"
	"net/http"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/graph"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/graph/generated"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/auth"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/api"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/configs"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/database"
	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/go-chi/cors"
)

func main() {
	/* create router with chi */
	var router = new(chi.Mux)
	router = chi.NewRouter()

	// Basic CORS
	cors := cors.New(cors.Options{
		AllowedOrigins:   []string{"*"},
		AllowedMethods:   []string{"GET", "POST", "OPTIONS"},
		AllowedHeaders:   []string{"Accept", "Authorization", "Content-Type", "X-CSRF-Token"},
		AllowCredentials: true,
		MaxAge:           300, // Maximum value not ignored by any of major browsers
	})
	router.Use(cors.Handler)

	/* sugguss middleware stack from chi */
	router.Use(middleware.RequestID)
	router.Use(middleware.RealIP)
	router.Use(middleware.Logger)
	router.Use(middleware.Recoverer)

	/* use auth middleware */
	router.Use(auth.Middleware)

	/* init database */
	database.InitDB()

	/* add rest api register and login */
	router.Route("/api", func(r chi.Router) {
		r.Post("/register", auth.Register)
		r.Post("/login", auth.Login)
		r.Get("/speech", api.Speech)
	})

	/* exec graphq schema and create server handler */
	executableSchema := generated.NewExecutableSchema(generated.Config{Resolvers: &graph.Resolver{}})
	server := handler.NewDefaultServer(executableSchema)

	/* router handler */
	router.Handle("/", playground.Handler("Harvest Vocabs", "/query"))
	router.Handle("/query", server)

	/* logging on start and listen */
	log.Printf("connect to http://localhost:%s/ for GraphQL playground", configs.PORT)
	err := http.ListenAndServe(":"+configs.PORT, router)
	if err != nil {
		panic(err)
	}
}
