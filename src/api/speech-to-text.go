package api

import (
	"context"
	"fmt"
	"net/http"

	texttospeech "cloud.google.com/go/texttospeech/apiv1"
	texttospeechpb "google.golang.org/genproto/googleapis/cloud/texttospeech/v1"
)

func Speech(rw http.ResponseWriter, r *http.Request) {
	// Instantiates a client.
	ctx := context.Background()
	text := r.URL.Query().Get("text")
	client, err := texttospeech.NewClient(ctx)
	if err != nil {
		fmt.Println("Error new client: ", err)
	}
	defer client.Close()
	// Perform the text-to-speech request on the text input with the selected
	// voice parameters and audio file type.
	req := texttospeechpb.SynthesizeSpeechRequest{
		// Set the text input to be synthesized.
		Input: &texttospeechpb.SynthesisInput{
			InputSource: &texttospeechpb.SynthesisInput_Text{Text: text},
		},
		// Build the voice request, select the language code ("en-US") and the SSML
		// voice gender ("neutral").
		Voice: &texttospeechpb.VoiceSelectionParams{
			LanguageCode: "en-US",
			SsmlGender:   texttospeechpb.SsmlVoiceGender_NEUTRAL,
		},
		// Select the type of audio file you want returned.
		AudioConfig: &texttospeechpb.AudioConfig{
			AudioEncoding: texttospeechpb.AudioEncoding_MP3,
		},
	}
	resp, err := client.SynthesizeSpeech(ctx, &req)
	if err != nil {
		fmt.Println("Error synthesizeSpeech: ", err)
	}
	rw.Header().Add("Content-Type", "audio/mp3")
	rw.Write(resp.AudioContent)
}
