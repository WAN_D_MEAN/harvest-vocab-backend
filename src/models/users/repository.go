package users

import (
	"context"
	"fmt"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/database"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func FindByEmail(ctx context.Context, email string) *UserModel {
	res := database.Users.FindOne(ctx, &UserModel{
		Email: email,
	})
	user := UserModel{}
	res.Decode(&user)
	return &UserModel{
		ID:       user.ID,
		Name:     user.Name,
		Email:    user.Email,
		Password: user.Password,
	}
}

func FindById(ctx context.Context, id string) (*UserModel, error) {
	ObjectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return &UserModel{}, fmt.Errorf("invalid object id")
	}
	res := database.Users.FindOne(ctx, &UserModel{
		ID: ObjectID,
	})
	user := UserModel{}
	res.Decode(&user)
	return &UserModel{
		ID:       user.ID,
		Name:     user.Name,
		Email:    user.Email,
		Password: user.Password,
	}, nil
}

func Save(ctx context.Context, input Register) (*UserModel, error) {
	// hash password before insert into database
	hashedPassword, err := HashPassword(input.Password)
	if err != nil {
		return &UserModel{}, err
	}
	// save input into database
	res, err := database.Users.InsertOne(ctx, &UserModel{
		Name:     input.Name,
		Email:    input.Email,
		Password: hashedPassword,
	})
	if err != nil {
		return &UserModel{}, err
	}
	return &UserModel{
		ID:    res.InsertedID.(primitive.ObjectID),
		Name:  input.Name,
		Email: input.Email,
	}, nil
}
