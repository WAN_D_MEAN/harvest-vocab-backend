package vocabularies

import (
	"context"
	"fmt"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/graph/model"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/database"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func FindById(ctx context.Context, id string) (*VocabularyModel, error) {
	ObjectID, err := primitive.ObjectIDFromHex(id)
	if err != nil {
		return &VocabularyModel{}, fmt.Errorf("invalid object id")
	}
	res := database.Users.FindOne(ctx, &VocabularyModel{
		ID: ObjectID,
	})
	vocab := VocabularyModel{}
	res.Decode(&vocab)
	return &VocabularyModel{
		ID:         vocab.ID,
		Word:       vocab.Word,
		Definition: vocab.Definition,
		Level:      vocab.Level,
		Category:   vocab.Category,
	}, nil
}

func Save(ctx context.Context, input []*model.NewVocab) ([]*model.Vocabulary, error) {
	vocabs := []interface{}{}
	for _, vocab := range input {
		vocabs = append(vocabs, vocab)
	}
	result, err := database.Vocabulary.InsertMany(ctx, vocabs)
	if err != nil {
		return []*model.Vocabulary{}, err
	}
	res := []*model.Vocabulary{}
	for i, id := range result.InsertedIDs {
		vocab := &model.Vocabulary{
			ID:         id.(primitive.ObjectID).Hex(),
			Word:       input[i].Word,
			Definition: input[i].Definition,
			Level:      input[i].Level,
			Category:   input[i].Category,
		}
		res = append(res, vocab)
	}
	return res, nil
}

func Random(ctx context.Context, size int) ([]*model.Vocabulary, error) {
	cur, err := database.Vocabulary.Aggregate(ctx, []bson.M{{"$sample": bson.M{"size": size}}})
	if err != nil {
		return nil, err
	}
	defer cur.Close(ctx)
	vocabs := []*model.Vocabulary{}
	for cur.Next(ctx) {
		var vocab bson.M
		if err = cur.Decode(&vocab); err != nil {
			return vocabs, err
		}
		vocabs = append(vocabs, &model.Vocabulary{
			ID:         vocab["_id"].(primitive.ObjectID).Hex(),
			Word:       vocab["word"].(string),
			Definition: vocab["definition"].(string),
			Level:      vocab["level"].(string),
			Category:   vocab["category"].(string),
		})
	}
	return vocabs, nil
}

func All(ctx context.Context, input model.FilterVocab) ([]*model.Vocabulary, error) {
	filter := bson.M{}
	if len(input.Category) > 0 {
		filter["category"] = bson.M{"$in": input.Category}
	}
	if len(input.Level) > 0 {
		filter["level"] = bson.M{"$in": input.Level}
	}
	cur, err := database.Vocabulary.Find(ctx, filter)
	if err != nil {
		return []*model.Vocabulary{}, err
	}
	defer cur.Close(ctx)
	vocabs := []*model.Vocabulary{}
	for cur.Next(ctx) {
		var vocab bson.M
		if err = cur.Decode(&vocab); err != nil {
			return vocabs, err
		}
		vocabs = append(vocabs, &model.Vocabulary{
			ID:         vocab["_id"].(primitive.ObjectID).Hex(),
			Word:       vocab["word"].(string),
			Definition: vocab["definition"].(string),
			Level:      vocab["level"].(string),
			Category:   vocab["category"].(string),
		})
	}
	return vocabs, nil
}
