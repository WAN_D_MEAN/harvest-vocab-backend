package vocabularies

import "go.mongodb.org/mongo-driver/bson/primitive"

// Vocabulary collection
type VocabularyModel struct {
	ID         primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	Word       string             `json:"word" bson:"_id,omitempty"`
	Definition string             `json:"definition" bson:"_id,omitempty"`
	Level      string             `json:"level" bson:"_id,omitempty"`
	Category   string             `json:"category" bson:"_id,omitempty"`
}
