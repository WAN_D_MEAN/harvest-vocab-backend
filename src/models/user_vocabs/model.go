package user_vocabs

import (
	"context"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/graph/model"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/database"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/models/users"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/models/vocabularies"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

func Save(ctx context.Context, input *AddNewVocab) ([]*model.UserVocabulary, error) {
	vocabObjectIDs := []primitive.ObjectID{}
	for _, vocabId := range input.VocabIDs {
		vocabObjectID, err := primitive.ObjectIDFromHex(vocabId)
		if err != nil {
			return []*model.UserVocabulary{}, err
		}
		vocabObjectIDs = append(vocabObjectIDs, vocabObjectID)
	}
	cur, err := database.Vocabulary.Find(ctx, bson.M{"_id": bson.M{"$in": vocabObjectIDs}})
	if err != nil {
		return []*model.UserVocabulary{}, err
	}
	insertValues := []interface{}{}
	vocabs := []*model.Vocabulary{}
	for cur.Next(ctx) {
		var vocab bson.M
		if err = cur.Decode(&vocab); err != nil {
			return []*model.UserVocabulary{}, err
		}
		vocabId := vocab["_id"].(primitive.ObjectID)
		vocabs = append(vocabs, &model.Vocabulary{
			ID:         vocabId.Hex(),
			Word:       vocab["word"].(string),
			Definition: vocab["definition"].(string),
			Level:      vocab["level"].(string),
			Category:   vocab["category"].(string),
		})
		insertValues = append(insertValues, &UserVocabModel{
			UserID:  input.User.ID,
			VocabID: vocabId,
		})
	}
	result, err := database.UserVocab.InsertMany(ctx, insertValues)
	if err != nil {
		return []*model.UserVocabulary{}, err
	}
	userVocabs := []*model.UserVocabulary{}
	for i, id := range result.InsertedIDs {
		userVocab := &model.UserVocabulary{
			ID: id.(primitive.ObjectID).Hex(),
			User: &model.User{
				ID:    input.User.ID.Hex(),
				Name:  input.User.Name,
				Email: input.User.Email,
			},
			Vocabulary: vocabs[i],
		}
		userVocabs = append(userVocabs, userVocab)
	}
	return userVocabs, nil
}

func All(ctx context.Context, user *users.UserModel) ([]*model.UserVocabulary, error) {
	cur, err := database.UserVocab.Find(ctx, bson.M{"_user": user.ID})
	if err != nil {
		return []*model.UserVocabulary{}, err
	}
	userVocabs := []*model.UserVocabulary{}
	for cur.Next(ctx) {
		var userVocab bson.M
		if err = cur.Decode(&userVocab); err != nil {
			return []*model.UserVocabulary{}, err
		}
		userVocabObjectId := userVocab["_id"].(primitive.ObjectID)
		vocabObjectId := userVocab["_vocabulary"].(primitive.ObjectID)
		vocab, err := vocabularies.FindById(ctx, vocabObjectId.Hex())
		if err != nil {
			return []*model.UserVocabulary{}, err
		}
		userVocabs = append(userVocabs, &model.UserVocabulary{
			ID: userVocabObjectId.Hex(),
			User: &model.User{
				ID:    user.ID.Hex(),
				Name:  user.Name,
				Email: user.Email,
			},
			Vocabulary: &model.Vocabulary{
				ID:         vocab.ID.Hex(),
				Word:       vocab.Word,
				Definition: vocab.Definition,
				Level:      vocab.Level,
				Category:   vocab.Category,
			},
		})
	}
	return userVocabs, nil
}
