package user_vocabs

import (
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/models/users"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// User Vocabularies collection
type UserVocabModel struct {
	ID      primitive.ObjectID `json:"id" bson:"_id,omitempty"`
	UserID  primitive.ObjectID `json:"userId" bson:"_user,omitempty"`
	VocabID primitive.ObjectID `json:"vocabId" bson:"_vocabulary,omitempty"`
}

// User Vocabularies type
type AddNewVocab struct {
	User     users.UserModel
	VocabIDs []string
}
