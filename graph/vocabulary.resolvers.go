package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/graph/model"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/models/vocabularies"
)

func (r *mutationResolver) NewVocabs(ctx context.Context, input []*model.NewVocab) ([]*model.Vocabulary, error) {
	res, err := vocabularies.Save(ctx, input)
	if err != nil {
		return []*model.Vocabulary{}, fmt.Errorf(err.Error())
	}
	return res, nil
}

func (r *queryResolver) Vocabs(ctx context.Context, input model.FilterVocab) ([]*model.Vocabulary, error) {
	res, err := vocabularies.All(ctx, input)
	if err != nil {
		return []*model.Vocabulary{}, fmt.Errorf(err.Error())
	}
	return res, nil
}

func (r *queryResolver) RandomVocabs(ctx context.Context, size int) ([]*model.Vocabulary, error) {
	res, err := vocabularies.Random(ctx, size)
	if err != nil {
		return []*model.Vocabulary{}, fmt.Errorf(err.Error())
	}
	return res, nil
}
