package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/graph/generated"
)

func (r *mutationResolver) Healthz(ctx context.Context) (string, error) {
	return "OK", nil
}

func (r *queryResolver) Healthz(ctx context.Context) (string, error) {
	return "OK", nil
}

// Mutation returns generated.MutationResolver implementation.
func (r *Resolver) Mutation() generated.MutationResolver { return &mutationResolver{r} }

// Query returns generated.QueryResolver implementation.
func (r *Resolver) Query() generated.QueryResolver { return &queryResolver{r} }

type mutationResolver struct{ *Resolver }
type queryResolver struct{ *Resolver }
