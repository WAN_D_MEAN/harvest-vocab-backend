package graph

// This file will be automatically regenerated based on the schema, any resolver implementations
// will be copied through when generating and any unknown code will be moved to the end.

import (
	"context"
	"fmt"

	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/graph/model"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/auth"
	"bitbucket.org/WAN_D_MEAN/harvest-vocab-backend/src/models/user_vocabs"
)

func (r *mutationResolver) NewUserVocabs(ctx context.Context, vocabIds []string) ([]*model.UserVocabulary, error) {
	user := auth.ForContext(ctx)
	if user == nil {
		return []*model.UserVocabulary{}, fmt.Errorf("access denied")
	}
	res, err := user_vocabs.Save(ctx, &user_vocabs.AddNewVocab{
		User:     *user,
		VocabIDs: vocabIds,
	})
	if err != nil {
		return []*model.UserVocabulary{}, err
	}
	return res, nil
}

func (r *queryResolver) UserVocabs(ctx context.Context) ([]*model.UserVocabulary, error) {
	user := auth.ForContext(ctx)
	if user == nil {
		return []*model.UserVocabulary{}, fmt.Errorf("access denied")
	}
	res, err := user_vocabs.All(ctx, user)
	if err != nil {
		return []*model.UserVocabulary{}, err
	}
	return res, nil
}
